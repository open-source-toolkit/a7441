# Video DownloadHelper 合作应用 2.0.19 版本下载

本仓库提供 Video DownloadHelper 浏览器插件的合作应用 2.0.19 版本的下载资源。该应用适用于 macOS 系统，并分别提供了适用于 Intel 芯片和 M 系列芯片的安装包。

## 资源文件列表

- **vdhcoapp-mac-x86_64-installerV2.0.19.pkg**  
  适用于 Intel 芯片的 macOS 系统。

- **vdhcoapp-mac-arm64-installerV2.0.19.pkg**  
  适用于 M 系列芯片的 macOS 系统。

## 使用说明

1. 根据您的 Mac 电脑的芯片类型（Intel 或 M 系列），选择相应的安装包进行下载。
2. 下载完成后，双击安装包文件进行安装。
3. 安装完成后，您可以配合 Video DownloadHelper 浏览器插件使用，以实现更便捷的视频下载功能。

## 注意事项

- 请确保您的 macOS 系统版本与安装包兼容。
- 安装过程中可能需要管理员权限，请确保您有足够的权限进行安装。

## 反馈与支持

如果您在使用过程中遇到任何问题或有任何建议，欢迎通过 GitHub 的 Issues 功能提交反馈。我们将尽快为您提供帮助。

感谢您使用 Video DownloadHelper 合作应用！